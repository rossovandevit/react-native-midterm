import React, {useState} from 'react';
import {View, TouchableWithoutFeedback, Text, StyleSheet} from 'react-native';

const OPACITY = {
    BLUR: 0.3,
    DEFAULT: 1,
  }

export default props => {
    const [opacity, setOpacity] = useState(1);
  return (
      <TouchableWithoutFeedback onPress={props.onClick}>
        <View
          onTouchStart={() => setOpacity(OPACITY.BLUR)}
          onTouchEnd={() => setOpacity(OPACITY.DEFAULT)}
          style={StyleSheet.flatten([styles.button, props.style, {opacity}])}>
          <Text style={props.textStyle}>{props.children}</Text>
        </View>
      </TouchableWithoutFeedback>
      
  );
};


const styles = StyleSheet.create({
    error: {
      color: 'white',
      backgroundColor: 'red',
    },
    button: {
      backgroundColor: '#F74FAE',
      padding: 10,
      borderRadius: 10,
      margin: 5,
      alignItems: 'center',
      elevation: 10,
      
    },
  });
