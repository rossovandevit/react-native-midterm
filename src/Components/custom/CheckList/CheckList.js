import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import checkIcon from './checkIcon.png';
import {CheckedItem, UncheckedItem} from './Item';

const CheckList = ({items, multiple, ...props}) => {
  const [checkedItem, setCheckedItem] = useState([]);

  const onCheckedItem = (item) => {
    if (checkedItem.includes(item)) {
      return setCheckedItem(
        checkedItem.filter((checkedItem) => checkedItem !== item),
      );
    }

    if (multiple) {
      setCheckedItem([...checkedItem, item]);
    } else {
      setCheckedItem([item]);
    }
  };
  return (
    <View {...props} style={StyleSheet.compose(styles.container, props.style)}>
      {items.map((item) => (
        <TouchableOpacity onPress={() => onCheckedItem(item)}>
          {checkedItem.includes(item) ? (
            <CheckedItem item={item} />
          ) : ( 
            <UncheckedItem item={item} />
          )}
        </TouchableOpacity>
      ))}
    </View>
  );
};
CheckList.defaultProps = {
  items: [],
};

export default CheckList;

const styles = StyleSheet.create({
  
  icon: {
    width: 25,
    height: 25,
  },

  item: {
    flexDirection: 'row',
    backgroundColor: 'orange',
    padding: 10,
    borderRadius: 2,
    margin: 5,
    fontSize: 25,
  },

  itemText: {
    color: 'white',
  },
});
