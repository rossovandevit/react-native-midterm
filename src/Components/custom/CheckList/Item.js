import React, {useState} from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import checkIcon from './checkIcon.png';

const CheckedItem = ({item}) => {
  return (
    <View style={styles.item}>
      <Image style={styles.icon} source={checkIcon} />
      <Text style={styles.itemText}> {' ' + item}</Text>
    </View>
  );
};
const UncheckedItem = ({item}) => {
  return (
    <View style={styles.item}>
      <Text>{'       '}</Text>
      <Text style={styles.itemText}> {' ' + item}</Text>
    </View>
  );
};
export {CheckedItem, UncheckedItem};

const styles = StyleSheet.create({
  icon: {
    width: 25,
    height: 25,
  },

  item: {
    flexDirection: 'row',
    backgroundColor: 'orange',
    padding: 10,
    borderRadius: 2,
    margin: 5,
    fontSize: 25,
  },

  itemText: {
    color: 'white',
  },
});
