import React, {useState} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import CheckList from './custom/CheckList';
import Button from './custom/Button';
import MyTextInput from './custom/TextInput';

export default () => {
  const clickme = () => alert('You have click me!!');
  const todoList = ['HomeWork', 'Meeting', 'Gym', 'Seminar'];
  const [checkedItem, setCheckedItem] = useState([]);

  return (
    <View style={{flexDirection: 'column'}}>
      <CheckList style={{margin: 1}} items={todoList} multiple={true} />

      <CheckList items={['asdf', 'sadfkj']} />

      <Button onClick={clickme}>Click me</Button>
      <Button
        onClick={clickme}
        style={{backgroundColor: 'red'}}
        textStyle={{color: 'white'}}>
        Show me
      </Button>

      <MyTextInput
        placeholder="Input your username"
        onChangeText={(text) => alert(text)}
      />
      <MyTextInput placeholder="First name" type="text" validate={true} />
      <MyTextInput placeholder="Last name" validate={true} />
      <MyTextInput placeholder="Input your phone number" type="number" />
    </View>
  );
};

const styles = StyleSheet.create({
  icon: {
    width: 25,
    height: 25,
  },

  item: {
    flexDirection: 'row',
    backgroundColor: 'orange',
    padding: 10,
    borderRadius: 10,
    margin: 5,
    elevation: 10,
    fontSize: 25,
  },

  itemText: {
    color: 'white',
  },
});
