import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  defaultFontSize: {
    fontSize: 15,
  },
});

export default styles;
