import React from 'react';
import {Text, View} from 'react-native';
import Styles from '../styles';
import Button from '../../Components/custom/Button';
import {SCREEN_NAME} from '../navigation';

export default (props) => {
  const back = () => {
    props.navigation.reset({
      index: 0,
      routes: [{name: SCREEN_NAME.LOGIN}],
    });
  };
  return (
    <View>
      <Text style={Styles.defaultFontSize}>About Us</Text>
      <Button onClick={back}>Reset</Button>
    </View>
  );
};

