import React, {useState} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Styles from '../styles';
import Button from '../../Components/custom/Button';
import TextInput from '../../Components/custom/TextInput';
import {SCREEN_NAME} from '../navigation';
import {useNavigation} from '@react-navigation/native';

export default (props) => {
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const login = () => {
    props.navigation.navigate(SCREEN_NAME.HOME, {
      user: username,
      pass: password,
    });
  };

  return (
    <View style={styles.container}>
      <TextInput placeholder="Username or Email" onChangeText={setUsername} />
      <TextInput
        placeholder="Password"
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button onClick={login}>Login</Button>
      <Text style={styles.textEditor}>Don't have any account?</Text>
      <Text style={styles.signUp}>Sign Up</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  textEditor: {
    fontSize: 14,
    textAlign: 'center',
    color: '#F74FAE',
  },
  signUp: {
    fontSize: 20,
    textAlign: 'center',
    color: '#F74FAE',
  },
  container: {
    flex: 1,
    margin: 20,
    paddingTop: 20,
    
  },
});
