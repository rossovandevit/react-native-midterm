import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import Styles from '../styles';
import Button from '../../Components/custom/Button';
import {SCREEN_NAME} from '../navigation';

export default (props) => {
  const back = () => {
    props.navigation.reset({
      index: 0,
      routes: [{name: SCREEN_NAME.LOGIN}],
    });
  };
  return (
    <View style={styles.container}>
      <Text style={Styles.textEditor}>
      Hello {props.route.params.user} Welcome IT support.If you have any problem you can contact this email Itsupport@gmail.com or this phone number 012856897
      </Text>
      {/* <Text style={Styles.defaultFontSize}>
        Password: {props.route.params.pass}{' '}
      </Text> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20,
    paddingTop: 20,
  },
  textEditor: {
    fontSize: 15,
    color: '#F74FAE',
  },
});
