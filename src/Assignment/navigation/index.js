import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../screen/Home';
import AboutUs from '../screen/AboutUs';
import Login from '../screen/Login';

const Stack = createStackNavigator();

export const SCREEN_NAME = {
  LOGIN: 'Login',
  HOME: 'Home',
  ABOUT_US: 'AboutUs',
};

export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName={SCREEN_NAME.LOGIN}
        screenOptions={{headerTitleAlign: 'center', headerTintColor: '#F74FAE'}}>
        <Stack.Screen
          name={SCREEN_NAME.HOME}
          component={Home}
          options={{title: 'Home Page'}}
        />
        <Stack.Screen
          name={SCREEN_NAME.LOGIN}
          component={Login}
          options={{title: 'Login'}}
        />
        <Stack.Screen name={SCREEN_NAME.ABOUT_US} component={AboutUs} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
